import React from "react";
import ReactDOM from "react-dom";
import ModalVideo from "react-modal-video";
import ShoutGeneral from "./assets/shout-general.jpg";
import ShoutAngy from "./assets/shout-angy.jpg";
import ShoutAlumni1 from "./assets/shout-alumni-1.jpg";
import ShoutAlumni2 from "./assets/shout-alumni-2.jpg";

class ShoutOuts extends React.Component {
    constructor() {
        super();
        this.state = {
            //note that every video added must have its own 'state' variable
            isHibbs: false,
            isAngy: false,
            isAlumni1: false,
            isAlumni2: false,
            isShout1: false,
            isShout2: false,
            isShout3: false,
            isShout4: false,
        };
    }

    render() {
        return (
            <div>
                <div className="shoutout-vids grid-x grid-margin-x">
                    <div className="thumbnail">
                        <img
                            src={ShoutGeneral}
                            className="shoutThumb"
                            onClick={() => this.setState({ isHibbs: true })}
                        />
                    </div>
                    <div className="thumbnail">
                        <img
                            src={ShoutAngy}
                            className="shoutThumb"
                            onClick={() => this.setState({ isAngy: true })}
                        />
                    </div>
                    <div className="thumbnail">
                        <img
                            src={ShoutAlumni1}
                            className="shoutThumb"
                            onClick={() => this.setState({ isAlumni1: true })}
                        />
                    </div>
                    <div className="thumbnail">
                        <img
                            src={ShoutAlumni2}
                            className="shoutThumb"
                            onClick={() => this.setState({ isAlumni2: true })}
                        />
                    </div>
                    <div className="thumbnail">
                        <img
                            src={ShoutGeneral}
                            className="shoutThumb"
                            onClick={() => this.setState({ isShout1: true })}
                        />
                    </div>
                    <div className="thumbnail">
                        <img
                            src={ShoutGeneral}
                            className="shoutThumb"
                            onClick={() => this.setState({ isShout2: true })}
                        />
                    </div>
                    <div className="thumbnail">
                        <img
                            src={ShoutGeneral}
                            className="shoutThumb"
                            onClick={() => this.setState({ isShout3: true })}
                        />
                    </div>
                    <div className="thumbnail">
                        <img
                            src={ShoutGeneral}
                            className="shoutThumb"
                            onClick={() => this.setState({ isShout4: true })}
                        />
                    </div>
                </div>
                <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isHibbs}
                    videoId="hwLylb5il9c"
                    onClose={() => this.setState({ isHibbs: false })}
                    controls="0"
                />
                <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isAngy}
                    videoId="EdqJH4TTuXk"
                    onClose={() => this.setState({ isAngy: false })}
                />
                <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isAlumni1}
                    videoId="ginkUdAy29o"
                    onClose={() => this.setState({ isAlumni1: false })}
                />
                <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isAlumni2}
                    videoId="PQq7VDM5mCs"
                    onClose={() => this.setState({ isAlumni2: false })}
                />
                <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isShout1}
                    videoId="e1DH-7BYy4o"
                    onClose={() => this.setState({ isShout1: false })}
                />
                <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isShout2}
                    videoId="M9LEDUe2QY4"
                    onClose={() => this.setState({ isShout2: false })}
                />
                <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isShout3}
                    videoId="UmFWEXKlcig"
                    onClose={() => this.setState({ isShout3: false })}
                />
                <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isShout4}
                    videoId="gL3hlcTFSJQ"
                    onClose={() => this.setState({ isShout4: false })}
                />
            </div>
        );
    }
}

export default ShoutOuts;
