import React from 'react';
import { StyleSheet, css } from 'aphrodite';

const styles = StyleSheet.create({
  root: {
    textAlign: 'center',
    backgroundColor: '#a30f32',
    color: '#ffffff',
    padding: '3em',
  },
});

function Footer() {
  return (
    <footer className={css(styles.root)}>
      <p>Central Washington University | 400 E. University Way, Ellensburg, WA 98926</p>
      <p>Campus Operator (509) 963-1111 | Public Affairs (509) 963-1221</p>
      <p>AA/EEO/Title IX/Veteran/Disability Employer</p>
    </footer>
  );
}

export default Footer;