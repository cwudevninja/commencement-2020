import React from 'react';
import clsx from 'clsx';
import Student from './Student';
import students from './students.json';
import honors from './honors.json';
import DiplomaModalProvider from './DiplomaModalProvider';

const findHonors = (first = '', middle = '', last = '') => {
  return honors.find((honor =>
      (honor.first || '').toLowerCase() === first.toLowerCase()
   && (middle ? (honor.middle || '').toLowerCase() === middle.toLowerCase() : true)
   && (honor.last || '').toLowerCase() === last.toLowerCase()
  ))?.honor
};

const StudentList = React.memo(function ({ filter }) {
  const filteredStudents = filter ? students.filter((s) => s.last[0] === filter) : [];

  console.log('Rerendering');

  return (
    <div className="item-student">
      <DiplomaModalProvider>
        {filteredStudents.map((s, index) => (
          <Student key={index} data={{
            ...s,
            honors: findHonors(s.first, s.middle, s.last)
          }} />
        ))}
      </DiplomaModalProvider>
    </div>
  );
});

export default StudentList;
