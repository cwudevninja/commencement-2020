import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import htmlToImage from 'html-to-image';
import Modal from 'react-modal';
import { saveAs } from 'file-saver';
import qs from 'qs';
import Diploma from './Diploma';
import { dataURItoBlob } from './utils';

Modal.setAppElement("#root");
Modal.defaultStyles.overlay.zIndex = 100;

const customStyles = {
  overlay: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.75)',
  },
  content : {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'static',
    padding: '30px',
    flexShrink: 1,
  }
};

const context = React.createContext();

function DiplomaModalProvider ({ children }) {
  const [student, setStudent] = React.useState({});
  const [open, setOpen] = React.useState(false);
  const [selectedFile, setSelectedFile] = React.useState({}); // file data
  const [picture, setPicture] = React.useState(null); // actual image data
  const [rendering, setRendering] = React.useState(false);
  const diplomaRef = React.useRef();

  const showDiploma = React.useCallback((student) => {
    setStudent(student);
    setOpen(true);
  }, []);

  const handleClose = () => {
    setOpen(false);
    setSelectedFile({});
    setPicture(null);
  };

  const handleFileChange = (event) => {
    if (!event.target.files[0]) return;

    if (event.target.files[0].size > 2097152) {
      alert("File is too big!");
      return;
    }
    event.persist();
    let fileReader = new FileReader();
    setSelectedFile(event.target.files[0]);

    // loads the file
    fileReader.onloadend = () => {
      setPicture(fileReader.result)
      localStorage.setItem('commencement2020-picture', fileReader.result)
      localStorage.setItem('commencement2020-pictureName', event.target.files[0].name)
    };

    fileReader.readAsDataURL(event.target.files[0]);
  };

  const exportDiploma = () => {
    // setRendering(true);
    setTimeout(() => {
      const node = ReactDOM.findDOMNode(diplomaRef.current);
  
      htmlToImage.toJpeg(node, {
        // width: 1000,
        // height: 1000 * 2550 / 3300,
      })
        .then(function (dataUrl) {
          const blob = new Blob([dataURItoBlob(dataUrl)], { type: 'image/jpeg' });
          saveAs(blob, `${student.data.first}_${student.data.last}.png`);
        })
        .catch(function (error) {
          console.error('oops, something went wrong!', error);
        });
      // setRendering(false);
    }, 1);
  }

  return (
    <>
      <context.Provider value={showDiploma}>
        {children}
      </context.Provider>
      <Modal
        isOpen={open}
        onRequestClose={handleClose}
        style={customStyles}
      >
        <Diploma
          ref={diplomaRef}
          {...student.data}
          picture={picture}
          pictureName={selectedFile.name}
          rendering={rendering}
        />
        <div className="diploma__modal_buttons">
          <div style={{
            display: 'flex',
            flexDirection: 'column',
            marginLeft: '17vh',
          }}>
            <label htmlFor="diploma_file_upload" className="button button__grey">Upload Photo</label>
            <input
              id="diploma_file_upload"
              type="file"
              accept="image/*"
              onChange={handleFileChange}
              style={{
                width: '500px'
              }}
              className="show-for-sr"
            />
            <p style={{
              margin: 0,
            }}>{selectedFile ? selectedFile.name : ''}</p>
          </div>
          <button
            onClick={() => exportDiploma()}
            className='button button__cwu'
          >Download Slide</button>
        </div>
      </Modal>
    </>
  )
}

export default DiplomaModalProvider;

export const useDiplomaModal = () => React.useContext(context);