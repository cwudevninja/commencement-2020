import React from 'react';
import { withRouter } from 'react-router';
import qs from 'qs';
import Diploma from './Diploma';

function DiplomaPage ({ location }) {
  const picture = localStorage.getItem('commencement2020-picture');
  const pictureName = localStorage.getItem('commencement2020-pictureNmae');
  const student = qs.parse(location.search, { ignoreQueryPrefix: true });

  return (
    <main>
      <Diploma
        {...student}
        picture={picture}
        pictureName={pictureName}
      />
    </main>
  )
}

export default withRouter(DiplomaPage);
