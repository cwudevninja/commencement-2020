export const sortBy = (key) => (a,b) => a[key] < b[key] ? 1 : (a[key] > b[key] -1 : 0);

export const formatMajor = (plan_descr) => plan_descr.indexOf('Major') > -1
  ? plan_descr.split(' ').slice(0,-1).join(' ') : plan_descr;

export const formatDegree = {
  BA: 'Bachelor of Arts',
  BS: 'Bachelor of Science',
  BAED: 'Bachelor of Education',
  BAS: 'Bachelor of Applied Science',
  BFA: 'Bachelor of Fine Arts',
  BMUS: 'Bachelor of Music',
  CERT: 'Certificate',
  EDS: 'Educational Specialist',
  MA: 'Master of Arts',
  MED: 'Master of Education',
  MMUS: 'Master of Music',
  MS: 'Master of Science',
  BSBA: 'BS, BA',
  NTCERT: '',
};

export const formatHonor = {
  CL: 'Cum Laude',
  MCL: 'Magna Cum Laude',
  SCL: 'Summa Cum Laude'
};

// https://stackoverflow.com/questions/46405773/saving-base64-image-with-filesaver-js
// Converts a dataUri to a Blob
// this lets us download images
export const dataURItoBlob = (dataURI) => {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  const byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

  // write the bytes of the string to an ArrayBuffer
  const ab = new ArrayBuffer(byteString.length);

  // create a view into the buffer
  const ia = new Uint8Array(ab);

  // set the bytes of the buffer to the correct values
  for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  const blob = new Blob([ab], { type: mimeString });
  return blob;
}