import React from 'react';
import { StyleSheet, css } from 'aphrodite';
import { useDiplomaModal } from './DiplomaModalProvider';
import { formatDegree, formatHonor } from './utils';

// first
// middle
// last
// career
// exp_grad
// degree
// acadm_plan
// plan_descr
// sub_plan_descr
// plan_type
// campus

const styles = StyleSheet.create({
  root: {
    borderTop: '1px solid #ccc',
    borderBottom: '1px solid #ccc',
    margin: '1em',
    padding: '1em',
    borderRadius: 4,
    cursor: 'pointer',

  },
  name: {
    textAlign: 'left',
    borderTop: 0,
    margin: 0,
    fontSize: '1.2rem',
  },
  major: {
    fontSize: '.8em',
    textAlign: 'left',
  },
  honors: {

  }
});

function Student({ data }) {
  const {
    first, middle, last, degree, plan_descr, sub_plan_descr, honors
  } = data;
  const showDiploma = useDiplomaModal();

  const major = plan_descr.indexOf('Major') > -1 ? plan_descr.split(' ').slice(0, -1).join(' ') : plan_descr;
  const aOrAn = sub_plan_descr && ['a', 'e', 'i', 'o', 'u'].indexOf(sub_plan_descr[0]) > -1 ? 'an' : 'a'
  const minor = sub_plan_descr ? (
    sub_plan_descr.indexOf('Specialization') > -1 ? (
      <>
        with {aOrAn} <b>{sub_plan_descr}</b>
      </>
    ) : (
      <>
        with a specialization in <b>{sub_plan_descr}</b>
      </>
    )
  ) : '';

  return (
    <div
      className="student"
      onClick={() => showDiploma({data})}
    >
      <p className={css(styles.name)}>{first} {middle} {last}</p>
      <p className={css(styles.major)}><b>{formatDegree[degree]}</b> in <b>{major}</b> {minor}</p>
      {honors && (<p className={'greekHonor ' + css(styles.honors)}><i class="fas fa-user-graduate"></i> {formatHonor[honors]}</p>)}
    </div>
  );
}

export default React.memo(Student);
