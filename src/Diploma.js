import React from 'react';
import EditableTextField from './EditableTextField';
import honors from './honors.json';
import { formatHonor } from './utils';

const findHonors = (first = '', middle = '', last = '') => {
  return honors.find((honor =>
      (honor.first || '').toLowerCase() === first.toLowerCase()
   && (middle ? (honor.middle || '').toLowerCase() === middle.toLowerCase() : true)
   && (honor.last || '').toLowerCase() === last.toLowerCase()
  ))?.honor
};

const Diploma = React.forwardRef(({
  rendering, first, middle, last, degree, plan_descr, picture, pictureName
}, ref) => {
  const major = plan_descr.indexOf('Major') > -1 ? plan_descr.split(' ').slice(0, -1).join(' ') : plan_descr;

  const honors = findHonors(first, middle, last);
  console.log(honors);

  return (
  <div id="diploma" ref={ref} style={{
    display: 'flex',
    justifyContent: 'center',
  }}>
      <div className="diploma__content-container">
        <div className="diploma__content">
          <p className="diploma__name">{first} {last}</p>
          {honors && (<p className="diploma__honor"><i class="fas fa-user-graduate"></i> {formatHonor[honors]}</p>)}
          <p className="diploma__degree">{degree} {major}</p>
          <div style={{
            width: '100%',
            height: '100px',
            display: 'flex',
          }}/>
          <div className="diploma__bottom">
            <div className="diploma__picture-container">
              <div style={{
                  backgroundImage: `url(${picture})`,
                  backgroundSize: 'cover'
                }}
                className="diploma__picture"
              />
            </div>
            <div className="diploma__note">
              <EditableTextField rendering={rendering}/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default Diploma;
