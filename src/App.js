import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import HomePage from './HomePage';
import DiplomaPage from './DiplomaPage';

const App = function () {
  return (
    <Router>
      <Switch>
        <Route path="/diploma/">
          <DiplomaPage />
        </Route>
        <Route path="/">
          <HomePage />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
