import React from "react";
import { StyleSheet, css } from "aphrodite";
import StudentList from "./StudentList";
import Footer from "./Footer";
import logo from "./assets/CWU_Signature_Stacked-RGB.png";
import cwu2020 from "./assets/cwu-commencement2020.png";
import cwuDiplomaIcon from "./assets/cwu-diploma icon.svg";
import banner from "./assets/graduate_banner.mp4";
import ShoutOuts from "./ShoutOuts";
import ShoutTitle from "./assets/shout-out-title.png";
import plyrCeremony from "./plyrCeremony";


const styles = StyleSheet.create({
    main: {
        backgroundColor: "#414141",
        height: "80vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "relative",
        color: "#fff",
    },
    index: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        paddingTop: "1em",
        paddingBottom: "1em",
        borderTop: "1px solid #BFBFBF",
        borderBottom: "1px solid #BFBFBF",
    },
    videoContainer: {
        position: "absolute",
        zIndex: 0,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: "100%",
        overflow: "hidden",
    },
    video: {
        opacity: 0.25,
        top: "50%",
        left: "50%",
        zIndex: -100,
        minWidth: "100%",
        minHeight: "100%",
    },
    front: {
        zIndex: 10,
    },
    logo: {
        position: "absolute",
        top: 0,
        left: 0,
    },
    classyLetterContainer: {
        textAlign: "center",
    },
    classyLetter: {
        position: "sticky",
        top: 0,
        fontFamily: "georgia, serif",
        fontSize: "6rem",
    },
});

const Letters = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
];

const HomePage = function () {
    const [filter, setFilter] = React.useState("");

    const handleChangeFilter = (letter) => {
        if (letter === filter) {
            setFilter("");
        } else {
            setFilter(letter);
        }
    };

    return (
        <main>
            <section>
                <div className="grid-x grid-padding-x align-center">
                    <div className="cell small-8 medium-12 brandingLogo">
                        <img src={logo} alt="Central Washington University" />
                    </div>
                    <div className="cell medium-6"></div>
                </div>
            </section>

            
            <section className={"hero " + css(styles.main)}>
            <div class="video-container">
                            <iframe src="https://www.youtube.com/embed/qqCh8faCp9Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture fullscreen" allowfullscreen
                            allowfullscreen="allowfullscreen"
                            mozallowfullscreen="mozallowfullscreen" 
                            msallowfullscreen="msallowfullscreen" 
                            oallowfullscreen="oallowfullscreen" 
                            webkitallowfullscreen="webkitallowfullscreen"></iframe>
                        </div> 
                <div className={css(styles.videoContainer)}>

                    {/* 
                        REMOVED FOR GO-LIVE DAY
                    <video loop muted autoPlay className={css(styles.video)}>
                        <source src={banner} />
                    </video>
                    */}
                </div>
                
                <div className={css(styles.front)}>
                    <div className="hero-content"> 
                    
                        {/* 
                            REMOVED FOR GO-LIVE DAY

                        <h1 style={{ zIndex: 10 }}>
                            <img
                                src={cwu2020}
                                style={{ width: "400px" }}
                                alt="CWU 2020 Commencement"
                            />
                        </h1>
                        <br />
                        <p>
                            Virtual Ceremony | Saturday, June 13 | 9:00 a.m. PST
                        </p>
                        */}
                    </div>
                </div>
            </section>

            <section className="commencement-info">
                <div className="grid-container grid-x">
                    <a
                        className="small-12 program"
                        href="https://online.flippingbook.com/view/559776/"
                        target="blank"
                    >
                        <img src={cwuDiplomaIcon} />
                        <br />
                        <span>Commencement Program (PDF)</span>
                    </a>
                </div>
            </section>

            <section className="graduate-names">
                <div className="grid-container">
                    <div className="grad-slide-text">
                    <div className="shout-out-title">
               <center> <img
                    src={ShoutTitle}
                    style={{ width: "750PX" }}
                    alt="CWU 2020 Commencement"
                /></center>
              </div>
              <div className="slideInst">
                        <h3>Create Your Graduate Slide</h3>
                        <p>Find and create your commencement slide (mobile devices are not supported):</p>
                        <ol>
                            <li>
                            Select a letter to find your name <strong>(alphabetized by last name).</strong></li>
                                <li>
                                Click your name for the commencement slide to appear.</li>
                                <li>
                                Upload an image to customize your slide <strong>(portrait images work best if less than 1MB and 600px wide by 800px tall).</strong></li>
                                <li>
                                You may write a message by typing directly on the text area to the right of your image.</li>
                                <li>
                                Once done, click "Download Slide" in the lower right to save your slide on your computer.</li>
                                <li>
                                Share your slide on social media using <span className="emph">#CWUGrad.</span>
                                </li>
</ol>
<p><strong>Congratulations on your accomplishment!</strong></p>
</div>
                    </div>
                    <div className="closeInstruction">
                        <a
                            href="#"
                            className="back-to-top"
                            style={{ display: "inline" }}
                        >
                            <span className="show-for-sr">Top</span>
                        </a>
                        <small>
                            <i className="fas fa-info-circle"></i> Click on the
                            letter again to close the list
                        </small>
                    </div>
                    <div className={css(styles.index)}>
                        <div className="letterParent">
                            {Letters.map((letter) => (
                                <button
                                    key={letter}
                                    className="letter"
                                    onClick={() => handleChangeFilter(letter)}
                                >
                                    {letter}
                                </button>
                            ))}
                        </div>
                    </div>

                    <div className="grid-x">
                        <div
                            className={`cell small-1 ${css(
                                styles.classyLetterContainer
                            )}`}
                        >
                            <span className={"classyLetter " + css(styles.classyLetter)}>
                                {filter}
                            </span>
                        </div>
                        <div className="cell small-11">
                            <StudentList filter={filter} />
                        </div>
                    </div>
                </div>
            </section>
            <section id="shoutouts" className="shout-outs">
            <p className="congrats">
            Your Wildcat family salutes the CWU Class of 2020. Props to you for all of your accomplishments.
                </p>
                
                <ShoutOuts />

            </section>
            <section className="social">
                <div className="grid-container">
                    <h3>#CWUGrad</h3>
                    <p>
                        Congratulations CWU Graduates. Share your accomplishment
                        by adding #CWUGrad to your social posts.
                    </p>
                </div>
            </section>

            <section
                className="social"
                style={{ paddingLeft: 0, paddingRight: 0 }}
            >
                <iframe
                    title="CWU Together"
                    src="https://www.juicer.io/api/feeds/cwucommencement/iframe?page=1&amp;gutter=0&amp;pages=1&amp;filter=Facebook,Instagram,Twitter"
                    frameBorder="0"
                    height="1200"
                />
            </section>

            <Footer />
        </main>
    );
};

export default HomePage;
