import React, { useState, useRef } from 'react';

function EditableTextField({
  rendering
}) {
  const [value, setValue] = useState('');
  const textAreaRef = useRef();

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  React.useEffect(() => {
    textAreaRef.current.style.height = '0px';
    const scrollHeight = textAreaRef.current.scrollHeight;
    textAreaRef.current.style.height = scrollHeight + 'px';
  }, [value]);

  return (
    <textarea
      placeholder={rendering ? '' : 'Click to write a personal note'}
      value={value}
      onChange={handleChange}
      ref={textAreaRef}
      style={{
        width: '100%',
        color: 'white',
        backgroundColor: '#00000000',
        fontStyle: 'italic',
      }}
    />
  );
}

export default EditableTextField;
