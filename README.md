This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This is a simple webapp to act as a landing page for virtual commencement 2020.

This exists as it's a performant way to work with 3000 graduates.

To deploy:

  - simply pull from master, install packages (`npm install`) and run `npm run build`
  - clean up the directory you'd like to place this app by deleting all of the files on the prod folder (/sites-other/commencement-2020)
  - Copy everything from build/ into the prod folder (/sites-other/commencement-2020).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.